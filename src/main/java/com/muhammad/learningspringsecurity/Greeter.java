package com.muhammad.learningspringsecurity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Greeter {

	
	@GetMapping("/greet")
	public String hello() {
		return "sup man";
	}
	
}
